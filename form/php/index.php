<html>
    <head>
        <title>Form</title>
    </head>
    <body>
        <form method="post">
            <p>
                <select name="movie">
                    <option value="">Select a movie</option>
                    <option>Minions</option>
                    <option>Furious 7</option>
                    <option>Twilight</option>
                    <option>Iron Man 3</option>
                    <option>Frozen</option>
                </select>
            </p>
            <p>
                <button type="submit">Submit</button>
            </p>
        </form>
        <h3>Verdict</h3>
        <p>The movie you selected was "Frozen".</p>
        <p>That movie rocks!</p>
    </body>
</html>
