<html>
    <head>
        <title>Basic</title>
    </head>
    <body>
        <h1>Hi there!</h1>
        <h3>The year is <?= date('Y'); ?>.</h3>
        <h3>1 + 2 = <?= 1 + 2; ?></h3>
        <h3><?= rand(1, 10); ?> is a number between 1 and 10.</h3>
    </body>
</html>
