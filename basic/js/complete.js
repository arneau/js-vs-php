const http = require('http');

const app = http.createServer((request, response) => {
    response.write(`
        <html>
            <head>
                <title>Basic</title>
            </head>
            <body>
                <h1>Hi there!</h1>
                <h3>The year is ${(new Date()).getFullYear()}.</h3>
                <h3>1 + 2 = ${1 + 2}</h3>
                <h3>${Math.ceil(Math.random() * 10)} is a number between 1 and 10.</h3>
            </body>
        </html>
    `);
    response.end();
});

app.listen(3000);
