const express = require('express');
const bodyparser = require('body-parser');
const mysql = require('mysql');

const app = express();

app.use(bodyparser.urlencoded({ extended: false }));

app.use((request, response) => {
    if (request.body.firstname && request.body.lastname) {
        const db = mysql.createConnection({
            host: '127.0.0.1',
            user: 'root',
            password: '',
            database: 'js_vs_php'
        });
        db.connect(() => {
            db.query(`INSERT INTO users (firstname, lastname) VALUES("${request.body.firstname}", "${request.body.lastname}");`, (error, result) => {
                response.write(`
                    <html>
                        <head>
                            <title>Form</title>
                        </head>
                        <body>
                            <form method="post">
                                <p>
                                    <input name="firstname" placeholder="Firstname" />
                                </p>
                                <p>
                                    <input name="lastname" placeholder="Lastname" />
                                </p>
                                <p>
                                    <button type="submit">Submit</button>
                                </p>
                            </form>
                            <h3>Result</h3>
                            <p>Thank you for registering "${request.body.firstname} ${request.body.lastname}".</p>
                            <p>Your user ID is ${result.insertId}.</p>
                        </body>
                    </html>
                `);
                response.end();
            });
        });
    } else {
        response.write(`
            <html>
                <head>
                    <title>Form</title>
                </head>
                <body>
                    <form method="post">
                        <p>
                            <input name="firstname" placeholder="Firstname" />
                        </p>
                        <p>
                            <input name="lastname" placeholder="Lastname" />
                        </p>
                        <p>
                            <button type="submit">Submit</button>
                        </p>
                    </form>
                    <h3>Result</h3>
                    <p>Thank you for registering "Ethan Hunt".</p>
                    <p>Your user ID is 7.</p>
                </body>
            </html>
        `);
        response.end();
    }
});

app.listen(3000);
