<html>
    <head>
        <title>Form</title>
    </head>
    <body>
        <form method="post">
            <p>
                <input name="firstname" placeholder="Firstname" />
            </p>
            <p>
                <input name="lastname" placeholder="Lastname" />
            </p>
            <p>
                <button type="submit">Submit</button>
            </p>
        </form>
        <h3>Result</h3>
        <p>Thank you for registering "Ethan Hunt".</p>
        <p>Your user ID is 7.</p>
    </body>
</html>
